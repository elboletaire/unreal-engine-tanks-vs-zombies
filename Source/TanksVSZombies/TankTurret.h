// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/ArrowComponent.h"
#include "PaperSpriteComponent.h"
#include "Kismet/GameplayStatics.h"
#include "TankStatics.h"
#include "Missile.h"
#include "TankPlayerController.h"
#include "TankTurret.generated.h"

UCLASS()
class TANKSVSZOMBIES_API ATankTurret : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATankTurret();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "TankTurret", meta = (AllowPrivateAccess = "true"))
	float YawSpeed;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "TankTurret")
	class ATank* Tank;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "TankTurret")
	TSubclassOf<AActor> Projectile;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	// Which way is the tank facing
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "TankTurret", meta = (AllowPrivateAccess = "true"))
	UArrowComponent* Direction;

	// Body sprite
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "TankTurret", meta = (AllowPrivateAccess = "true"))
	UPaperSpriteComponent* Sprite;


	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "TankTurret", meta = (AllowPrivateAccess = "true"))
	float Fire1Cooldown;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "TankTurret", meta = (AllowPrivateAccess = "true"))
	float Fire1ReadyTime;
};
