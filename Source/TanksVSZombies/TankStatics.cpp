// Fill out your copyright notice in the Description page of Project Settings.

#include "TankStatics.h"

bool UTankStatics::FindLookAtAngle2D(const FVector2D& Start, const FVector2D& Target, float& Angle)
{
	FVector2D Normal = (Target - Start).GetSafeNormal();

	if (!Normal.IsNearlyZero())
	{
		Angle = FMath::RadiansToDegrees(FMath::Atan2(Normal.Y, Normal.X));
		// I may be missing a rotation value somewhere and that's why I force here 90 degrees; otherwise the turret does not point to the cursor.
		Angle += 90.0f;

		return true;
	}

	return false;
}
