// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TanksVSZombiesGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class TANKSVSZOMBIES_API ATanksVSZombiesGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
