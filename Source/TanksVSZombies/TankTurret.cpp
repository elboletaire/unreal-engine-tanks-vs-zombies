// Fill out your copyright notice in the Description page of Project Settings.

#include "TankTurret.h"
#include "Tank.h"

// Sets default values
ATankTurret::ATankTurret()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = Direction = CreateDefaultSubobject<UArrowComponent>("Direction");

	Sprite = CreateDefaultSubobject<UPaperSpriteComponent>("Sprite");
	Sprite->AttachTo(Direction);

	YawSpeed = 100.0f;
	Fire1Cooldown = 100.0f;
}

// Called when the game starts or when spawned
void ATankTurret::BeginPlay()
{
	Super::BeginPlay();

	Tank = Cast<ATank>(GetParentComponent()->GetOwner());
	check(Tank);
}

// Called every frame
void ATankTurret::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	check(Direction);
	if (Tank)
	{
		if (APlayerController* Controller = Cast<APlayerController>(Tank->GetController()))
		{
			FVector2D AimLocation;
			if (Controller->GetMousePosition(AimLocation.X, AimLocation.Y))
			{
				FVector2D TurretLocation = FVector2D::ZeroVector;
				UGameplayStatics::ProjectWorldToScreen(Controller, Direction->GetComponentLocation(), TurretLocation);

				float DesiredYaw;

				if (UTankStatics::FindLookAtAngle2D(TurretLocation, AimLocation, DesiredYaw))
				{
					FRotator CurrentRotation = Direction->GetComponentRotation();
					float DeltaYaw = FMath::FindDeltaAngleDegrees(CurrentRotation.Yaw, DesiredYaw);
					float MaxDeltaYawThisFrame = YawSpeed * DeltaTime;
					if (MaxDeltaYawThisFrame > FMath::Abs(DeltaYaw))
					{
						CurrentRotation.Yaw = DesiredYaw;
					}
					else
					{
						CurrentRotation.Yaw += (FMath::Sign(DeltaYaw) * MaxDeltaYawThisFrame);
					}
					Direction->SetWorldRotation(CurrentRotation);
				}
			}

			const FTankInput& CurrentInput = Tank->GetCurrentInput();
			if (CurrentInput.bFire1 && Projectile)
			{
				if (UWorld* World = GetWorld())
				{
					float CurrentTime = World->GetTimeSeconds();
					if (Fire1ReadyTime <= CurrentTime)
					{
						if (AActor* NewProjectile = World->SpawnActor(Projectile))
						{
							FVector Loc = Sprite->GetSocketLocation("Muzzle");
							FRotator Rot = Sprite->GetComponentRotation();
							NewProjectile->SetActorLocation(Loc);
							NewProjectile->SetActorRotation(Rot);
						}

						Fire1ReadyTime = CurrentTime + Fire1Cooldown;
					}
				}
			}
		}
	}
}
