// Fill out your copyright notice in the Description page of Project Settings.

#include "Tank.h"

void FTankInput::Sanitize()
{
	// Ensure units move in a range from -1 to 1
	MovementInput = RawMovementInput.ClampAxes(-1.0f, 1.0f);
	MovementInput = MovementInput.GetSafeNormal();
	RawMovementInput.Set(0.0f, 0.0f);
}

void FTankInput::MoveY(float AxisValue)
{
	RawMovementInput.Y += AxisValue;
}

void FTankInput::Fire1(bool bPressed)
{
	bFire1 = bPressed;
}

void FTankInput::Fire2(bool bPressed)
{
	bFire2 = bPressed;
}

void FTankInput::MoveX(float AxisValue)
{
	RawMovementInput.X += AxisValue;
}

// Sets default values
ATank::ATank()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Speed = 100.0f;
	YawSpeed = 180.0f;

	if (!RootComponent) {
		RootComponent = CreateDefaultSubobject<USceneComponent>("TankBase");
	}

	Direction = CreateDefaultSubobject<UArrowComponent>("Direction");
	Direction->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepWorldTransform);

	Sprite = CreateDefaultSubobject<UPaperSpriteComponent>("Sprite");
	Sprite->AttachToComponent(Direction, FAttachmentTransformRules::KeepWorldTransform);

	Turret = CreateDefaultSubobject<UChildActorComponent>("Turret");
	Turret->AttachToComponent(Direction, FAttachmentTransformRules::KeepWorldTransform);

	SpringArm = CreateDefaultSubobject<USpringArmComponent>("SpringArm");
	SpringArm->TargetArmLength = 500.0f;
	SpringArm->bEnableCameraLag = true;
	SpringArm->CameraLagSpeed = 2.0f;
	// We're not rotating our camera
	SpringArm->bEnableCameraRotationLag = false;
	SpringArm->bAbsoluteRotation = true;
	
	// We don't care about our character's rotation
	SpringArm->bUsePawnControlRotation = false;
	// Disable collisions for spring arm, as there won't be
	SpringArm->bDoCollisionTest = false;
	
	SpringArm->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepWorldTransform);
	// Set arm/camera on top of the tank
	SpringArm->AddWorldRotation(FRotator(-90.0f, 0.0f, 0.0f));

	Camera = CreateDefaultSubobject<UCameraComponent>("Camera");
	Camera->bUsePawnControlRotation = false;
	Camera->bAbsoluteRotation = false;
	// Set camera as orthographic
	Camera->ProjectionMode = ECameraProjectionMode::Orthographic;
	Camera->OrthoWidth = 1024.0f;
	Camera->AspectRatio = 3.0f / 4.0f;
	// Attach camera to the spring arm end joint (via SocketName)
	Camera->AttachToComponent(SpringArm, FAttachmentTransformRules::KeepWorldTransform, USpringArmComponent::SocketName);
	Camera->SetWorldRotation(FRotator(-90.0f, -90.0f, 0.0f));

}

// Called when the game starts or when spawned
void ATank::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ATank::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	TankInput.Sanitize();
	// UE_LOG(LogTemp, Warning, TEXT("Movement: (%f %f)"), TankInput.MovementInput.X, TankInput.MovementInput.Y);

	{
		FVector DesiredMovementDirection = FVector(TankInput.MovementInput.X, TankInput.MovementInput.Y, 0.0f);
		if (!DesiredMovementDirection.IsNearlyZero())
		{
			// Rotate the tank. Note that we move the direction component, not the tank itself.
			FRotator MovementAngle = DesiredMovementDirection.Rotation();
			float DeltaYaw = FMath::FindDeltaAngleDegrees(Direction->GetComponentRotation().Yaw, MovementAngle.Yaw);
			bool bReverse = false;

			if (DeltaYaw != 0.0f)
			{
				float AdjustedDeltaYaw = DeltaYaw;
				if (AdjustedDeltaYaw < -90.0f)
				{
					AdjustedDeltaYaw += 180.0f;
					bReverse = true;
				}
				else if (AdjustedDeltaYaw > 90.0f)
				{
					AdjustedDeltaYaw -= 180.0f;
					bReverse = true;
				}

				float MaxYawThisFrame = YawSpeed * DeltaTime;
				if (MaxYawThisFrame >= FMath::Abs(AdjustedDeltaYaw))
				{
					if (bReverse)
					{
						FRotator FacingAngle = MovementAngle;
						FacingAngle.Yaw = MovementAngle.Yaw + 180.0f;
						Direction->SetWorldRotation(FacingAngle);
					}
					else
					{
						Direction->SetWorldRotation(MovementAngle);
					}
				}
				else
				{
					// Can't reach our desireed angle this frame, rotate part way (?)
					Direction->AddLocalRotation(FRotator(0.0f, FMath::Sign(AdjustedDeltaYaw) * MaxYawThisFrame, 0.0f));
				}
			}

			{
				FVector MovementDirection = Direction->GetForwardVector() * (bReverse ? -1.0f : 1.0f);
				FVector Pos = GetActorLocation();

				Pos.X += MovementDirection.X * Speed * DeltaTime;
				Pos.Y += MovementDirection.Y * Speed * DeltaTime;

				SetActorLocation(Pos);
			}
		}
	}
}

// Called to bind functionality to input
void ATank::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveX", this, &ATank::MoveX);
	PlayerInputComponent->BindAxis("MoveY", this, &ATank::MoveY);
	PlayerInputComponent->BindAction("Fire1", EInputEvent::IE_Pressed, this, &ATank::Fire1Pressed);
	PlayerInputComponent->BindAction("Fire1", EInputEvent::IE_Released, this, &ATank::Fire1Released);
	PlayerInputComponent->BindAction("Fire2", EInputEvent::IE_Pressed, this, &ATank::Fire2Pressed);
	PlayerInputComponent->BindAction("Fire2", EInputEvent::IE_Released, this, &ATank::Fire2Released);
}

void ATank::MoveX(float AxisValue)
{
	TankInput.MoveX(AxisValue);
}

void ATank::MoveY(float AxisValue)
{
	TankInput.MoveY(AxisValue);
}

void ATank::Fire1Pressed()
{
	TankInput.Fire1(true);
}

void ATank::Fire1Released()
{
	TankInput.Fire1(false);
}

void ATank::Fire2Pressed()
{
	TankInput.Fire2(true);
}

void ATank::Fire2Released()
{
	TankInput.Fire2(false);
}

